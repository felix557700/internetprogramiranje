window.onload = function () {
    var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf('/')+1).split(".")[0];
    var element = document.getElementById(id);

    if(element != null)
        element.setAttribute("class", element.getAttribute("class") + " active");
}
