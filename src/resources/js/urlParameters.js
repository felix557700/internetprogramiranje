
function addQueryParameter(newParametar) {
    var urlQuery = window.location.search.substring(1);
    var urlParameters = urlQuery.split("&");
    var queryBuilder = "?";

    for (var i = 0; i < urlParameters.length; i++) {
        var pair = urlParameters[i].split("=");
        var pairOption = newParametar.split("=");

        if ((pair != null) && (pairOption != null) && (pair[0] == pairOption[0])) {
            var found = true;
            urlParameters[i] = newParametar;
        }

        if (pair[0] != "id")
            queryBuilder += urlParameters[i];

        if (i < urlParameters.length - 1)
            queryBuilder += "&";
    }

    if (!found) {
        if (queryBuilder.length > 1)
            queryBuilder += "&";

        queryBuilder += newParametar;
    }

    window.location.search = queryBuilder;
}
