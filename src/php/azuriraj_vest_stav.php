<?php

    $image_path = "\\InternetProgramiranje\\src\\resources\\img\\news-img\\";
    $path = __DIR__ . "\\..\\resources\\img\\news-img\\";

    $tip = $_POST["tip"];
    $IDTeksta = $_POST["IDTeksta"];

    include_once("connect.php");

    if(isset($_POST["submit"]))
    {
        // upload file
        if(isset($_FILES["novaslika"]) && !empty($_FILES["novaslika"]["name"]))
        {
            if(is_uploaded_file($_FILES["novaslika"]["tmp_name"]))
            {
                $isUploaded = move_uploaded_file($_FILES["novaslika"]["tmp_name"], $path . $_FILES["novaslika"]["name"]);
                $slika = $image_path . $_FILES["novaslika"]["name"];
            }
        }
        else
        {
            $slika = $_POST["slika"];
        }

        $naslov = htmlentities(trim($_POST["naslov"]));

        //////////////////////////////////////////////////////////////////////////////
        // filtriranje samo teksta
        $tekst = (trim($_POST["tekst"]));
        $search = array(
            '/<script[^>]*?>.*?<\/script>/si',
            '/<iframe[^>]*?>.*?<\/iframe>/si',
        );

        $tekst = preg_replace($search, "", $tekst);
        //////////////////////////////////////////////////////////////////////////////

        $linkovi = htmlentities(trim($_POST["linkovi"]));
        $autor = htmlentities(trim($_POST["autor"]));
        $email = htmlentities(trim($_POST["email"]));

        if(empty($naslov) || empty($slika) || empty($tekst) || empty($linkovi) || empty($autor) || empty($email))
        {
            echo "bad input";
            return;
        }
        else
        {
            $naslov = $connection->real_escape_string($naslov);
            $slika = $connection->real_escape_string($slika);
            $tekst = $connection->real_escape_string($tekst);
            $linkovi = $connection->real_escape_string($linkovi);
            $autor = $connection->real_escape_string($autor);
            $email = $connection->real_escape_string($email);

            if($tip === "vest")
            {
                $sql = "UPDATE vesti
                        SET naslov = '$naslov', slika = '$slika', tekst = '$tekst', linkovi = '$linkovi', autor = '$autor', email = '$email'
                        WHERE IDVesti = $IDTeksta";
            }
            elseif($tip === "stav")
            {
                $sql = "UPDATE stavovi
                    SET naslov = '$naslov', slika = '$slika', tekst = '$tekst', linkovi = '$linkovi', autor = '$autor', email = '$email'
                    WHERE IDStava = $IDTeksta";
            }

            $connection->query($sql);
        }
    }
    elseif(isset($_POST["clear"]))
    {
        //prvo brise sve komentare za taj stav/vest ako ih ima
        $sql = "DELETE
                FROM komentari
                WHERE tip = '$tip' AND IDTeksta = $IDTeksta";

        $connection->query($sql);

        if($tip === "vest")
        {
            $sql = "DELETE
                    FROM vesti
                    WHERE IDVesti = $IDTeksta";
        }
        elseif($tip === "stav")
        {
            $sql = "DELETE
                    FROM stavovi
                    WHERE IDStava = $IDTeksta";
        }

        $connection->query($sql);
    }
    else
    {
        echo "error submit";
        return;
    }

    $connection->close();

    // Redirect
    header("Location: index.php");
?>
