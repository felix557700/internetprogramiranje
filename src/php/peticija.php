<!DOCTYPE html>
<html>
    <?php include_once("zaglavlje.php"); ?>

    <body id="top">
        <?php include_once("navigacija.php"); ?>

        <div class="site-wrap">
            <h1>Peticija protiv prodaje Telekoma Srbija</h1>
            <div>
                <p>Poštovani sugrađani Republike Srbije,<br/>
                    U našem vlasništvu, odnosno vlasnšitvu države Srbije ostalo je još malo vrednih kompanija,sve ostalo je rasprodato ili uništeno pljačkaškim privatizacijama i stečajima. Prodaja Telekoma znači odlivanje velikih sredstava iz Srbije, čije će posledice biti dalje smanjenje broja radnih mesta i smanjenje plata i penzija, povećanje budžetskog i spoljno-trgovinskog deficita.<br/><br/>
                    Molim vas sve da potpišete peticiju protiv prodaje giganta koga posedujemo kao država i da bar na taj način ostavimo nešto generacijama koje dolaze!
                </p>
            </div>

            <a href="unos-potpisa.php" class="btn btn-primary">Potpiši</a>
            <a href="lista-potpisa.php" class="btn btn-primary">Lista potpisa</a>
        </div>

        <?php include("footer.php"); ?>
    </body>
</html>
