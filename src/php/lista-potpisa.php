<!DOCTYPE html>
<html>
    <?php include_once("zaglavlje.php"); ?>

    <body id="top">
        <?php include_once("navigacija.php"); ?>

        <div class="site-wrap">
            <h1>Lista potpisa</h1>
            <?php
                include_once("connect.php");

                $sql = "SELECT ime, prezime
                        FROM potpisnici;";

                $result = $connection->query($sql);

                if ($result->num_rows > 0)
                {
                    echo "<ol class='petition-list'>";

                    while($row = $result->fetch_assoc())
                    {
                        echo "<li>". $row["ime"]. " " .$row["prezime"] ."</li>";
                    }
                    echo "</ol>";
                }
                else
                {
                    echo "Nema potpisa";
                }
                $connection->close();
            ?>
        </div>

        <?php include_once("footer.php"); ?>
    </body>
</html>