<div class="nav">
    <ul>
        <li id="index"><a href="index.php" class="active">Naslovna</a></li>
        <li id="o-nama"><a href="o-nama.php">O nama</a></li>
        <li id="peticija"><a href="peticija.php">Peticija</a></li>
        <li id="vesti"><a href="vesti.php">Vesti</a>
            <ul>
                <li><a href="prodaja.php">Prodaja</a></li>
                <li><a href="ne-prodaji.php">Ne prodaji</a></li>
                <li><a href="akcije.php">Akcije</a></li>
                <li><a href="analize.php">Analize</a></li>
            </ul>
        </li>
        <li id="stavovi"><a href="stavovi.php">Stavovi</a>
            <ul>
                <li><a href="strucnjaci.php">Stručnjaci</a></li>
                <li><a href="vlast.php">Vlast</a></li>
                <li><a href="protivnici.php">Protivnici prodaje</a></li>
                <li><a href="pristalice.php">Pristalice prodaje</a></li>
            </ul>
        </li>
        <li id="akcije"><a href="akcije.php">Akcije</a></li>
        <li id="unos-vesti-stava"><a href="unos-vesti-stava.php">Unos vesti/stava</a></li>
        <li id="unos-potpisa"><a href="unos-potpisa.php">Unos potpisa</a></li>
        <li id="kontakt"><a href="kontakt.php">Kontakt</a></li>
        <?php
            session_start();

            $isAdmin = false;
            if( isset($_SESSION["korisnik"]) && ($_SESSION["korisnik"] == "ADMIN") )
                $isAdmin = true;


            if ($isAdmin)
            {
                echo <<< EOT
                <li id="azuriraj"><a href="azuriraj.php">Ažuriraj</a></li>
                <li id="logout"><a href="logout.php">Izloguj se</a></li>
EOT;
            }
            else
            {
                echo <<< EOT
                <li id="login"><a href="login.php">Uloguj se</a></li>
EOT;
            }
        ?>
    </ul>
</div>
