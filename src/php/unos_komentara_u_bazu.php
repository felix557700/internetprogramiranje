<?php
    if(isset($_POST["submit"]))
    {
        $tip = htmlentities(trim($_POST["tip"]));
        $IDTeksta = htmlentities(trim($_POST["IDTeksta"]));
        $ime = htmlentities(trim($_POST["ime"]));
        $komentar = htmlentities(trim($_POST["komentar"]));

        if(empty($tip) || empty($IDTeksta) || empty($ime) || empty($komentar))
        {
            echo "bad input";
            return;
        }
        else
        {
            include_once("connect.php");

            $tip = $connection->real_escape_string($tip);
            $IDTeksta = $connection->real_escape_string($IDTeksta);
            $ime = $connection->real_escape_string($ime);
            $komentar = $connection->real_escape_string($komentar);

            $sql = "INSERT INTO komentari (tip, IDTeksta, ime, komentar)
                    VALUES ('$tip', '$IDTeksta', '$ime', '$komentar');";

            $connection->query($sql);

            if($tip === "vest")
            {
                $sql = "UPDATE vesti
                        SET brojKomentara = brojKomentara + 1
                        WHERE IDVesti = $IDTeksta";
            }
            elseif($tip === "stav")
            {
                $sql = "UPDATE stavovi
                        SET brojKomentara = brojKomentara + 1
                        WHERE IDStava = $IDTeksta";
            }

            $connection->query($sql);

            $connection->close();

            // Redirect
            header("Location: " . $_SERVER["HTTP_REFERER"]);
        }
    }
?>
