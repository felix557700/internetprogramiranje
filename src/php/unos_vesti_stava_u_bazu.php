<?php
    $image_path = "\\InternetProgramiranje\\src\\resources\\img\\news-img\\";
    $path = __DIR__ . "\\..\\resources\\img\\news-img\\";

    if(isset($_POST["submit"]))
    {
        $opcija = htmlentities(trim($_POST["vest_ili_stav"]));
        $kategorija = htmlentities(trim($_POST["kategorija"]));

        $naslov = htmlentities(trim($_POST["naslov"]));
        $slika = $image_path . $_FILES["slika"]["name"];


        //////////////////////////////////////////////////////////////////////////////
        // filtriranje samo teksta
        $tekst = (trim($_POST["tekst"]));
        $search = array(
            '/<script[^>]*?>.*?<\/script>/si',
            '/<iframe[^>]*?>.*?<\/iframe>/si',
        );

        $tekst = preg_replace($search, "", $tekst);
        //////////////////////////////////////////////////////////////////////////////

        $linkovi = htmlentities(trim($_POST["linkovi"]));
        $autor = htmlentities(trim($_POST["autor"]));
        $email = htmlentities(trim($_POST["email"]));

        if(empty($opcija) || empty($kategorija) || empty($naslov) || empty($slika) || empty($tekst) || empty($linkovi) || empty($autor) || empty($email))
        {
            echo "bad input";
            return;
        }
        else
        {
            include_once("connect.php");

            $naslov = $connection->real_escape_string($naslov);
            $slika = $connection->real_escape_string($slika);
            $tekst = $connection->real_escape_string($tekst);
            $linkovi = $connection->real_escape_string($linkovi);
            $autor = $connection->real_escape_string($autor);
            $email = $connection->real_escape_string($email);

            if($opcija === "vest")
            {
                $sql = "INSERT INTO vesti (kategorija, naslov, slika, tekst, linkovi, autor, email, vreme)
                        VALUES ('$kategorija', '$naslov', '$slika', '$tekst', '$linkovi', '$autor', '$email', now());";
            }
            elseif($opcija == "stav")
            {
                $sql = "INSERT INTO stavovi (kategorija, naslov, slika, tekst, linkovi, autor, email, vreme)
                        VALUES ('$kategorija', '$naslov', '$slika', '$tekst', '$linkovi', '$autor', '$email', now());";
            }

            if($connection->query($sql) == false)
            {
                echo "Error: Greska pri unosu podataka";
                return;
            }


            //  upload file
            if(isset($_FILES["slika"]) && !empty($_FILES["slika"]["name"]))
                if(is_uploaded_file($_FILES["slika"]["tmp_name"]))
                    $isUploaded = move_uploaded_file($_FILES["slika"]["tmp_name"], $path . $_FILES["slika"]["name"]);

            $connection->close();
            header("Location: index.php");
        }
    }
    else
    {
        echo "error submit";
        header("Location: unos-vesti-stava.php");
    }

?>
