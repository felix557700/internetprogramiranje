<!DOCTYPE html>
<html>
    <?php include_once("zaglavlje.php"); ?>

    <body id="top">
        <?php include_once("navigacija.php"); ?>

        <div class="site-wrap">
            <h1>Ažuriraj</h1>
            <?php
                if(isset($_POST["azuriraj"]))
                {
                    $tip = $_POST["tip"];
                    $IDTeksta = $_POST["IDTeksta"];

                    include_once("connect.php");

                    if($tip === "vest")
                    {
                        $sql = "SELECT *
                                FROM vesti
                                WHERE IDVesti = $IDTeksta";
                    }
                    elseif($tip === "stav")
                    {
                        $sql = "SELECT *
                                FROM stavovi
                                WHERE IDStava = $IDTeksta";
                    }

                    $result = $connection->query($sql);

                    if ($result->num_rows === 1)
                    {
                        $row = $result->fetch_assoc();
                        $IDTeksta = ($tip === "vest") ? $row["IDVesti"] : $row["IDStava"];
                        $naslov = $row["naslov"];
                        $slika = $row["slika"];
                        $tekst = $row["tekst"];
                        $linkovi = $row["linkovi"];
                        $autor = $row["autor"];
                        $email = $row["email"];

                        echo <<< EOT
                        <form action="azuriraj_vest_stav.php" method="post" enctype="multipart/form-data" style="width:500px; margin:0 auto">
                            <input type="hidden" name="tip" value="$tip"/>
                            <input type="hidden" name="IDTeksta" value="$IDTeksta"/>

                            <div>Naslov:</div>
                            <input class="form-control" type="text" name="naslov" value="$naslov"/><br/>

                            <div>Slika (ako se ne izabere nova slika, stara slika ostaje sačuvana):</div>
                            <input type="hidden" name="slika" value="$slika"/>
                            <input class="form-control" type="file" name="novaslika" accept="image/*"/><br/>

                            <div>Tekst:</div>
                            <textarea class="form-control" rows="10" cols="80" name="tekst" required>$tekst</textarea><br/>

                            <div>Linkovi:</div>
                            <textarea class="form-control" rows="5" cols="80" name="linkovi" required>$linkovi</textarea><br/>

                            <div>Autor:</div>
                            <input class="form-control" type="text" name="autor" autocomplete required value="$autor"/><br/>

                            <div>Email:</div>
                            <input class="form-control" type="email" name="email" autocomplete required value="$email"><br/>

                            <br/>
                            <input class="btn btn-primary" type="submit" name="submit" value="Ažuriraj">
                            <input class="btn btn-danger" type="submit" name="clear" value="Obriši">
                        </form>
EOT;
                    }
                    $connection->close();
                }
            ?>
        </div>

        <?php include_once("footer.php"); ?>
    </body>
</html>
