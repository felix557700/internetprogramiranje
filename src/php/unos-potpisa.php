<!DOCTYPE html>
<html>
    <?php include_once("zaglavlje.php"); ?>

    <body id="top">
        <?php include_once("navigacija.php"); ?>

        <div class="site-wrap">
            <h1>Unos potpisa</h1>
            <form action="unos_potpisa_u_bazu.php" method="post" style="width: 500px; margin: 0 auto" onsubmit="return proveri()">
                <div>Ime:</div>
                <input class="form-control" type="text" name="ime" autocomplete required/><br/>

                <div>Prezime:</div>
                <input class="form-control" type="text" name="prezime" autocomplete required/><br/>

                <div>Email:</div>
                <input class="form-control" type="email" name="email" autocomplete required/><br/>

                <div>JMBG:</div>
                <input class="form-control" type="text" name="jmbg" autocomplete required/><br/>

                <div>Mobilni:</div>
                <input class="form-control" type="text" name="mobilni" id="mobilni" autocomplete required/><br/>

                <div>Komentar:</div>
                <textarea class="form-control" rows="5" cols="80" name="komentar" required></textarea><br/>

                <input class="btn btn-primary" type="submit" name="submit" value="Potpiši">
                <input class="btn btn-danger" type="reset" name="reset" value="Poništi">
            </form>
        </div>

        <script>
            function proveri(){
                var mobilni = document.getElementById("mobilni").value;
                if(!mobilni.match("[0-9]{3}-[0-9]{3}([0-9]{3,4}|-[0-9]{4}|[0-9]-[0-9]{3})")){
                    alert("Niste uneli broj mobilnog telefona u ispravnom formatu! Format izgleda ovako xxx-xxxxxx, xxx-xxxxxxx, xxx-xxx-xxxx ili  xxx-xxxx-xxx Molim vas pokusajte ponovo.");
                    return false;
                }
                return true;
            }
        </script>

        <?php include("footer.php"); ?>
    </body>
</html>
