<!DOCTYPE html>
<html>
    <?php include_once("zaglavlje.php"); ?>

    <body id="top">
        <?php include_once("navigacija.php"); ?>

        <div class="site-wrap">
            <h1>O nama</h1>
            <p>Administratori ovog sajta su diplomirani inženjeri elektrotehnike koji su upoznati sa situacijom iz oblasti telekomunikacija i zato smo posebno zabrinuti za budućnost informaciono komunikacione industrije, kao i obrazovanja budućih generacija u ovoj oblasti. Budućnost elektrotehničkih i elektronskih fakulteta u Srbiji, koji su poznati i priznati svuda u svetu, bi bila ugrožna prodajom Telekoma.</p>
            <p>Želimo da izrazimo protivljenje da se državni kapital prepusti stranim firmama, jer bi to imalo nesagledive posledice po privredu, ekonomiju, obrazovanje i razvoj Srbije. Dovelo bi do masovnog smanjenja radne snage, podigla bi se cena usluga i osiromašio budžet Srbije.</p>
            <p>Pažljivo pročitajte tekst na stranici <a href="http://www.ains.rs/vesti.php?vise=TRUE&id=127">Akademije inženjerskih nauka Srbije</a></p>
        </div>

        <?php include("footer.php"); ?>
    </body>
</html>
