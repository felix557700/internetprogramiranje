<!DOCTYPE html>
<html>
    <?php include_once("zaglavlje.php"); ?>

    <body id="top">
        <?php include_once("navigacija.php"); ?>

    <div class="site-wrap">
            <h1>Kontakt</h1>

            <div class="" style="width: 755px;height: 600px;margin: 0 auto">
                <div class="admin">
                    <h2>Administratori sajta:</h2>
                    <div>Ivana Tanasić</div>
                    <div>E-mail: <a href="mailto:ivanatanasic@gmail.com" title="Click to send me an email">ivanatanasic@gmail.com</a></div>
                    <hr/>
                    <div>Filip Vitas</div>
                    <div>E-mail: <a href="mailto:vf143221m@student.etf.rs" title="Click to send me an email">vf143221m@student.etf.rs</a></div>
                    <hr/>
                    <div>Posetite nas: <a href="http://www.etf.bg.ac.rs">www.etf.bg.ac.rs</a></div>
                </div>

                <div class="mail">
                    <h2>Pošaljite mail:</h2>
                    <form action="mailto:ivanatanasic@gmail.com?Subject=Komentar" method="post" enctype="text/plain">
                        Vaše ime: <br/>
                        <input class="form-control" type="text" name="name" autocomplete required/><br/>
                        Vaš E-mail: <br/>
                        <input class="form-control" type="text" name="email" autocomplete required/><br/>
                        Komentar: <br/>
                        <textarea class="form-control" rows="10" cols="50" name="comment" required></textarea>
                        <br/>
                        <input class="btn btn-primary" type="submit" value="Pošalji">
                        <input class="btn btn-danger" type="reset" value="Poništi">
                    </form>
                </div>
            </div>
        </div>

        <?php include("footer.php"); ?>
    </body>
</html>
