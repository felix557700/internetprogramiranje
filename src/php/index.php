<!DOCTYPE html>
<html>
    <?php include_once("zaglavlje.php"); ?>

    <body id="top">
        <?php include_once("navigacija.php"); ?>

        <div class="site-wrap">
            <h1>Reci NE prodaji Telekoma!</h1>
            <div id="primary">
                <img id="home-img" src="../resources/img/telekom.jpg" alt="telekom"/>

                <div>
                    <p>Ne dozvolimo da prodaju nacionalne i strateške resurse, jedine koji su nam preostali.<br/>
                        Generacije unazad su ulagale u telekomunikacije! Činjenica je da je Telekoma najprofitabilnije preduzeće i jedno od
                        retkih preduzeća koje puni budžet Srbije. Najlakše je prodati jedno profitabilno preduzeće i uzeti pare. Ali,
                        osmisliti ekonomski sistem koji će izmiriti dug predstavlja nešto što je misaona imenica. I pored jake inostrane
                        konkurencije, Telenora i VIP-a, Telekom je i dalje neprikosnoveni lider na tržištu telekomunikacija u Srbiji.
                        Ako je uprava Telekoma bila toliko sposobna da zadrži Telekom na prvom mestu, zašto bi bio problem da se sa takvom
                        praksom i nastavi? Iako se već sada zna da će prodaja Telekoma Srbiji doneti velike probleme u budućnosti, svi ćute.
                        Mi nećemo i zato ovde od više razmatranih aspekata predstavljamo četiri grupe pitanja:</p>
                    <ol>
                        <li>Strateški značaj Telekoma</li>
                        <li>Ekonomske posledice prodaje Telekoma</li>
                        <li>Uticaj prodaje na razvoj telekomunikacija i informaciono-komunikacionih tehnologija (IKT) u Srbiji</li>
                        <li>Zakonska validnost prodaje Telekoma</li>
                    </ol>
                </div>
            </div>

            <div class="app secondary-class">
                <div class="date" id="date"></div>
                <div class="time" id="time"></div>
            </div>

            <?php
                include_once("connect.php");
                echo <<< EOT
                    <div class="app secondary-class">
                        <div class="date">BROJ POTPISNIKA PETICIJE:</div>
                        <div class="time">

EOT;
                $sql = "SELECT COUNT(*) AS broj
                        FROM potpisnici";

                $result = $connection->query($sql);
                $data = $result->fetch_assoc();
                $broj_potpisnika = $data["broj"];

                echo "<span>$broj_potpisnika</span>
                    </div></div>";

            ?>

            <div class="well" id="secondary">
                <div class="news-item">
                    <h3>Najnovije vesti</h3>
                </div>
                <hr/>

                <?php
                    include_once("connect.php");
                    $sql = "SELECT IDVesti, naslov
                            FROM vesti
                            ORDER BY IDVesti DESC
                            LIMIT 10";

                    $result = $connection->query($sql);

                    if ($result->num_rows > 0)
                    {
                        while ($row = $result->fetch_assoc())
                        {
                            $naslov = $row["naslov"];
                            $IDVesti = $row["IDVesti"];
                            echo <<< EOT
                                <div class="news-item">
                                    <a href="vesti.php?id=$IDVesti">$naslov</a>
                                </div>
                                <hr/>
EOT;
                        }
                    }

                    $connection->close();
                ?>
            </div><!--well-->
        </div>

        <script>
            var clock = setInterval("tick()", 1000);

            function tick() {
                var date    = new Date(),
                    year    = date.getFullYear(),
                    month   = (date.getMonth() + 1 > 9) ? date.getMonth() + 1 : "0" + (date.getMonth() + 1),
                    day     = (date.getDate()      > 9) ? date.getDate()      : "0" + date.getDate(),
                    seconds = (date.getSeconds()   > 9) ? date.getSeconds()   : "0" + date.getSeconds(),
                    minutes = (date.getMinutes()   > 9) ? date.getMinutes()   : "0" + date.getMinutes(),
                    hours   = (date.getHours()     > 9) ? date.getHours()     : "0" + date.getHours();

                document.getElementById('date').innerHTML = day + "." + month + "." + year + ".";
                document.getElementById('time').innerHTML = hours + ":" + minutes + ":" + seconds;
            }
        </script>

        <?php include_once("footer.php"); ?>
    </body>
</html>
