<!DOCTYPE html>
<html>
    <?php include_once("zaglavlje.php"); ?>

    <body id="top">
        <?php include_once("navigacija.php");
            if( isset($_SESSION["korisnik"]) && $_SESSION["korisnik"] === "ADMIN")
            {
                header("Location: index.php");
            }
        ?>

        <div class="site-wrap">
            <h1>Uloguj se</h1>
            <form action="provera.php" method="post" style="width: 500px; margin: 0 auto">
                <div>Username:</div>
                <input class="form-control" type="text" name="username" autocomplete required value="admin"/><br/>

                <div>Password:</div>
                <input class="form-control" type="password" name="password" autocomplete required value="admin"/><br/>

                <input class="btn btn-primary" type="submit" name="submit" value="Uloguj se">
                <input class="btn btn-danger" type="reset" name="reset" value="Poništi">
            </form>
        </div>

        <?php include_once("footer.php"); ?>
    </body>
</html>
