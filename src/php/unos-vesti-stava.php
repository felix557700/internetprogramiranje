<!DOCTYPE html>
<html>
    <?php include_once("zaglavlje.php"); ?>

    <body id="top">
        <?php include_once("navigacija.php"); ?>

        <div class="site-wrap">
            <h1>Unos vesti/stava</h1>

                <?php $option = (isset($_POST["option"])) ? $_POST["option"] : 0; ?>

                <form action="" method="post" style="width: 500px; margin: 0 auto">
                    <div>Izaberite vest ili stav:</div>
                    <select name="option" class="form-control" onchange="this.form.submit()">
                        <option hidden <?php if( !isset($option) ) echo 'selected'; ?>></option>
                        <option value="1" <?php if( isset($option) && ($option == 1) ) echo 'selected'; ?>>Vest</option>
                        <option value="2" <?php if( isset($option) && ($option == 2) ) echo 'selected'; ?>>Stav</option>
                    </select>
                </form>
                <br/>

            <form action="unos_vesti_stava_u_bazu.php" method="post" style="width: 500px; margin: 0 auto" enctype="multipart/form-data">
                <div>Izaberite kategoriju:</div>
                <?php
                    if($option == 1)
                    {
                        echo <<< EOT
                            <input type="hidden" name="vest_ili_stav" value="vest"/>
                            <select class="form-control" name="kategorija" required>
                                <option hidden></option>
                                <option value="prodaja">Prodaja</option>
                                <option value="ne_prodaji">Ne prodaji</option>
                                <option value="akcije">Akcije</option>
                                <option value="analize">Analize</option>
                            </select>
EOT;
                    }
                    elseif($option == 2)
                    {
                        echo <<< EOT
                            <input type="hidden" name="vest_ili_stav" value="stav"/>
                            <select class="form-control" name="kategorija" required>
                                <option hidden></option>
                                <option value="strucnjaci">Stručnjaci</option>
                                <option value="vlast">Vlast</option>
                                <option value="protivnici">Protivnici</option>
                                <option value="pristalice">Pristalice</option>
                            </select>
EOT;
                    }
                    else
                    {
                        echo <<< EOT
                        <select class="form-control" required>
                            <option></option>
                        </select>
EOT;
                    }
                ?>
                <br/>

                <div>Naslov:</div>
                <input class="form-control" type="text" name="naslov" required/><br/>

                <div>Slika:</div>
                <input class="form-control" type="file" name="slika" accept="image/*" required/><br/>

                <div>Tekst:</div>
                <textarea class="form-control" rows="10" cols="80" name="tekst" required></textarea><br/>

                <div>Linkovi:</div>
                <textarea class="form-control" rows="5" cols="80" name="linkovi" required></textarea><br/>

                <div>Autor:</div>
                <input class="form-control" type="text" name="autor" autocomplete required/><br/>

                <div>Email:</div>
                <input class="form-control" type="email" name="email" autocomplete required/><br/>

                <br/>
                <input class="btn btn-primary" type="submit" name="submit" value="Napravi">
                <input class="btn btn-danger" type="reset" name="reset" value="Poništi">
            </form>
        </div>

        <?php include("footer.php"); ?>
    </body>
</html>
