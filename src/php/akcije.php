<!DOCTYPE html>
<html>
    <?php include_once("zaglavlje.php"); ?>

    <body id="top">
        <?php include_once("navigacija.php"); ?>

        <div class="site-wrap">
            <h1>Akcije</h1>

            <hr/>
            <div style="display: inline-block;">
                <span>Sortiraj vesti:</span>
                <select class="form-control" onchange="addQueryParameter(this.options[this.selectedIndex].value)" style="display: inline-block; width: 200px;">
                    <option value="" hidden <?php if( !isset($_GET["sort"]) ) echo 'selected'; ?>></option>
                    <option value="sort=comments" <?php if( isset($_GET["sort"]) && ($_GET["sort"] == "comments") ) echo 'selected'; ?>>po broju komentara</option>
                    <option value="sort=likes" <?php if( isset($_GET["sort"]) && ($_GET["sort"] == "likes") ) echo 'selected'; ?>>po broju svidjanja</option>
                    <option value="sort=dislikes" <?php if( isset($_GET["sort"]) && ($_GET["sort"] == "dislikes") ) echo 'selected'; ?>>po broju nesvidjanja</option>
                </select>
            </div>

            <span>&nbsp;</span>

            <div style="display: inline-block;">
                <span>Vesti po stranici:</span>
                <select class="form-control" onchange="addQueryParameter(this.options[this.selectedIndex].value)" style="display: inline-block; width: 200px">
                    <option value="" hidden <?php if( !isset($_GET["limit"]) ) echo 'selected'; ?>></option>
                    <option value="limit=5" <?php if( isset($_GET["limit"]) && ($_GET["limit"] == "5")) echo 'selected'; ?>>5</option>
                    <option value="limit=10" <?php if( isset($_GET["limit"]) && ($_GET["limit"] == "10")) echo 'selected'; ?>>10</option>
                    <option value="limit=25" <?php if( isset($_GET["limit"]) && ($_GET["limit"] == "25")) echo 'selected'; ?>>25</option>
                    <option value="limit=50" <?php if( isset($_GET["limit"]) && ($_GET["limit"] == "50")) echo 'selected'; ?>>50</option>
                    <option value="limit=100" <?php if( isset($_GET["limit"]) && ($_GET["limit"] == "100")) echo 'selected'; ?>>100</option>
                </select>
            </div>

            <hr/>

            <?php
                include_once("connect.php");

                //default values for pagination
                $items_per_page = 5;
                $current_page = 1;

                $sql = "SELECT *
                        FROM vesti
                        WHERE kategorija = 'akcije'";

                if(isset($_GET["id"]) && is_numeric($_GET["id"]))
                {
                    $sql .= " AND IDVesti = " . $_GET['id'];
                }
                else
                {
                    if(isset($_GET["sort"]))
                    {
                        switch($_GET["sort"])
                        {
                            case "comments":
                                $sql .= " ORDER BY brojKomentara DESC";
                                break;

                            case "likes":
                                $sql .= " ORDER BY brojSvidjanja DESC";
                                break;

                            case "dislikes":
                                $sql .= " ORDER BY brojNesvidjanja DESC";
                                break;

                            default:
                                echo "greska";
                                return;
                        }
                    }
                    else
                    {
                        $sql .= " ORDER BY IDVesti DESC";
                    }

                    if(isset($_GET["page"]) && is_numeric($_GET["page"]))
                        $current_page = $_GET["page"];

                    if (isset($_GET["limit"]) && is_numeric($_GET["limit"]))
                        $items_per_page = $_GET["limit"];


                    $start_from = ($current_page - 1) * $items_per_page;
                    $sql .= " LIMIT $start_from, $items_per_page";
                }

                $result = $connection->query($sql);

                if ($result->num_rows > 0)
                {
                    while($row = $result->fetch_assoc())
                    {
                        $IDVesti = $row["IDVesti"];
                        $naslov = $row["naslov"];
                        $slika = $row["slika"];
                        $tekst = $row["tekst"];

                        //linkove unositi razdvojene zarezom
                        $linkovi = explode(",", $row["linkovi"]);

                        $autor = $row["autor"];
                        $brojSvidjanja = $row["brojSvidjanja"];
                        $brojNesvidjanja = $row["brojNesvidjanja"];
                        $brojKomentara = $row["brojKomentara"];
                        $vreme = date('d.m.Y H:i',strtotime($row["vreme"]));

                        echo <<< EOT
                            <div class="news">
                                <div class="heading"><h2>$naslov</h2></div>
                                <div>$vreme &nbsp; | &nbsp; $autor</div>
                                <div class="content-body">
                                    <p><img class="news-img" src="$slika">$tekst</p>
EOT;
                        foreach($linkovi as $link)
                        {
                            $naziv_linka = explode("//", $link)[1];
                            $naziv_linka = explode("/", $naziv_linka)[0];
                            echo "<div><a href='$link'>$naziv_linka</a></div>";
                        }

                        echo <<< EOT
                            </div>
                            <form action="like_dislike.php" method="POST" class="rating-form">
                                <input type="hidden" name="tip" value="vest"/>
                                <input type="hidden" name="IDTeksta" value="$IDVesti"/>
                                <button class="button-transparent" name="like" value="brojSvidjanja"><img src="../resources/img/up.png" class="rating-image"/></button>
                                <span class="rating-number">$brojSvidjanja</span>
                                <button class="button-transparent" name="dislike" value="brojNesvidjanja"><img src="../resources/img/down.png" class="rating-image"/></button>
                                <span class="rating-number">$brojNesvidjanja</span>
                            </form>
EOT;
                        if( isset($_SESSION["korisnik"]) && ($_SESSION["korisnik"] === "ADMIN") )
                        {
                            echo <<< EOT
                                <form action="azuriraj.php" method="post" style="display:inline;">
                                    <input type="hidden" name="tip" value="vest"/>
                                    <input type="hidden" name="IDTeksta" value="$IDVesti"/>
                                    <button class="pull-right btn btn-danger" name="azuriraj" style="margin-left: 10px;">Ažuriraj vest</button>
                                </form>
EOT;
                        }


                        $sql = "SELECT *
                                FROM komentari
                                WHERE tip='vest' AND IDTeksta='$IDVesti'";

                        $comments = $connection->query($sql);

                        echo "<p>Komentari ($brojKomentara):</p>";
                        echo '<div class="news-comment-list">';
                        if ($comments->num_rows > 0)
                        {
                            while ($comment = $comments->fetch_assoc())
                            {
                                $IDKomentara = $comment["IDKomentara"];
                                $ime = $comment["ime"];
                                $komentar = $comment["komentar"];
                                $like_number = $comment["brojSvidjanja"];
                                $dislike_number = $comment["brojNesvidjanja"];

                                echo <<< EOT
                                    <div class="panel panel-default">
                                        <div class="panel-heading">$ime
EOT;
                                if( isset($_SESSION["korisnik"]) && ($_SESSION["korisnik"] === "ADMIN") )
                                {
                                    echo <<< EOT
                                        <form action="obrisi_komentar.php" method="post" style="display:inline;">
                                            <input type="hidden" name="tip" value="vest"/>
                                            <input type="hidden" name="IDTeksta" value="$IDVesti"/>
                                            <input type="hidden" name="IDKomentara" value="$IDKomentara"/>
                                            <button class="pull-right button-transparent" name="obrisi" style="margin-left: 10px; cursor: pointer">&#x2716;</button>
                                        </form>
EOT;
                                }
                                echo <<< EOT
                                        <form action="like_dislike_komentari.php" method="post" class="pull-right">
                                            <input type="hidden" name="tip" value="vest"/>
                                            <input type="hidden" name="IDKomentara" value="$IDKomentara"/>
                                            <button class="button-transparent" name="like" value="brojSvidjanja"><img src="../resources/img/up.png" class="rating-image"/></button>
                                            <span class="rating-number">$like_number</span>
                                            <button class="button-transparent" name="dislike" value="brojNesvidjanja"><img src="../resources/img/down.png" class="rating-image"/></button>
                                            <span class="rating-number">$dislike_number</span>
                                        </form>
                                    </div>
                                    <div class="panel-body">$komentar</div>
                                </div>
EOT;
                            }
                        }
                        echo <<< EOT
                            <form action="unos_komentara_u_bazu.php" method="post">
                                <div class="panel panel-default">
                                    <input type="hidden" name="tip" value="vest"/>
                                    <input type="hidden" name="IDTeksta" value="$IDVesti"/>
                                    <div class="panel-heading"><input class="form-control" type="text" name="ime" placeholder="Vaše ime" style="width: inherit" required/></div>
                                    <div class="panel-body"><textarea class="form-control" rows="3" cols="10" name="komentar" placeholder="Komentar" required></textarea></div>
                                    <input class="btn btn-primary news-submit-comment" type="submit" name="submit" value="Pošalji komentar">
                                </div>
                            </form>
                            </div>
                        </div>
                        <hr/><br/>
EOT;
                    }

                    //PAGINATION
                    $sql_pagination = " SELECT COUNT(*) as total
                                        FROM vesti
                                        WHERE kategorija = 'akcije'";

                    $result = $connection->query($sql_pagination);
                    $row = $result->fetch_assoc();
                    $total_items = $row["total"];
                    $total_pages = ceil($total_items / $items_per_page);

                    $prev_page = $current_page - 1;
                    $next_page = $current_page + 1;
                    $pagination_number = 9;

                    if ($total_pages < $pagination_number)
                        $pagination_number = $total_pages;

                    $start_page = $current_page - floor($pagination_number / 2);
                    $end_page = $current_page + floor($pagination_number / 2);

                    if ($start_page < 1)
                    {
                        $end_page -= ($start_page - 1);
                        $start_page = 1;
                    }

                    if ($end_page > $total_pages - 1)
                    {
                        $start_page -= ($end_page - $total_pages);
                        $end_page = $total_pages;
                    }


                    echo "<div style='text-align: center'><ul class='pagination pagination-lg'>";

                    if($current_page == 1)
                    {
                        echo "<li class='disabled'><a><span>First</span></a></li>";
                        echo "<li class='disabled'><a><span>&laquo; Previous</span></a></li>";
                    }
                    else
                    {
                        echo "<li><a href='javascript:addQueryParameter(\"page=1\")'><span>First</span></a></li>";
                        echo "<li><a href='javascript:addQueryParameter(\"page=$prev_page\")'><span>&laquo; Previous</span></a></li>";
                    }

                    $page_number = ($start_page != 0) ? $start_page : 1;

                    while($page_number <= $end_page)
                    {
                        if($page_number == $current_page)
                            echo "<li class='active'><a>".$page_number."</a></li>";
                        else
                            echo "<li><a href='javascript:addQueryParameter(\"page=$page_number\")'>" . $page_number . "</a></li>";

                        $page_number++;
                    }

                    if($current_page == $total_pages)
                    {
                        echo "<li class='disabled'><a><span>Next &raquo;</span></a></li>";
                        echo "<li class='disabled'><a><span>Last</span></a></li>";
                    }
                    else
                    {
                        echo "<li><a href='javascript:addQueryParameter(\"page=$next_page\")'><span>Next &raquo;</span></a></li>";
                        echo "<li><a href='javascript:addQueryParameter(\"page=$total_pages\")'><span>Last</span></a></li>";
                    }
                    echo "</ul></div>";
                }
                else
                {
                    echo "Nema vesti";
                }

                $connection->close();
            ?>

        </div>

        <?php include("footer.php"); ?>
    </body>
</html>
