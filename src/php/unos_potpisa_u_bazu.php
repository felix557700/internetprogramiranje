<?php
    if(isset($_POST["submit"]))
    {
        $ime = htmlentities(trim($_POST["ime"]));
        $prezime = htmlentities(trim($_POST["prezime"]));
        $email = htmlentities(trim($_POST["email"]));
        $jmbg = htmlentities(trim($_POST["jmbg"]));
        $mobilni = htmlentities(trim($_POST["mobilni"]));
        $komentar = htmlentities(trim($_POST["komentar"]));

        if(!preg_match("/[0-9]{3}-[0-9]{3}([0-9]{3,4}|-[0-9]{4}|[0-9]-[0-9]{3})/", $mobilni))
        {
            echo "<p>Niste uneli broj mobilnog telefona u ispravnom formatu!</p>
                  <p>Format izgleda ovako xxx-xxxxxx, xxx-xxxxxxx, xxx-xxx-xxxx ili  xxx-xxxx-xxx</p>
                  <p>Molim vas pokusajte ponovo.</p>
                  <br/>
                  <a href='unos-potpisa.php'>Nazad na formular</a>";

            return;
        }

        if(empty($ime) || empty($prezime) || empty($email) || empty($jmbg) || empty($mobilni) || empty($komentar))
        {
            echo "bad input";
            return;
        }
        else
        {
            include_once("connect.php");

            $ime = $connection->real_escape_string($ime);
            $prezime = $connection->real_escape_string($prezime);
            $email = $connection->real_escape_string($email);
            $jmbg = $connection->real_escape_string($jmbg);
            $mobilni = $connection->real_escape_string($mobilni);
            $komentar = $connection->real_escape_string($komentar);


            $sql = "SELECT IDPotpisnika
                    FROM potpisnici
                    WHERE jmbg = '$jmbg';";

            $result = $connection->query($sql);

            if ($result->num_rows > 0)
            {
                $result->free();
                echo "duplicate";
                return;
            }

            $sql = "INSERT INTO potpisnici (ime, prezime, email, jmbg, mobilni, komentar)
                    VALUES ('$ime', '$prezime', '$email', '$jmbg', '$mobilni', '$komentar');";

            $connection->query($sql);
            $connection->close();
        }
    }
    else
    {
        echo "error";
        return;
    }

    // Redirect
    header("Location: index.php");
?>
